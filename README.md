# thinkster-django-angular-boilerplate with Django and AngularJS tutorial form https://thinkster.io/brewer/angular-django-tutorial/

## Installation

*NOTE: Requires [virtualenv]

* Fork this repository.
* `$ git clone git@bitbucket.org:<your-username>/thinkster-django-angular-boilerplate.git`
* `$ mkvirtualenv thinkster-djangular`
* `$ cd thinkster-django-angular-boilerplate/`
* `$ pip install -r requirements.txt`
* `$ python manage.py migrate`
* `$ python manage.py syncdb`
* `$ python manage.py runserver`

